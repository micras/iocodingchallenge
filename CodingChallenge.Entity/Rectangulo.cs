﻿using CodingChallenge.Common;

namespace CodingChallenge.Entity
{
    public class Rectangulo : FormaBase
    {
        public Rectangulo(decimal _base, decimal altura)
        {
            this.Base = _base;
            this.Altura = altura;
        }
        public override short FormaTypeId { get; set; } = Constant.Rectangulo;
        public virtual decimal Base { get; set; }
        public virtual decimal Altura { get; set; }

    }
}
