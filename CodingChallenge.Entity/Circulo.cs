﻿using CodingChallenge.Common;

namespace CodingChallenge.Entity
{
    public class Circulo : FormaLado
    {
        public Circulo(decimal lado)
        {
            this.Lado = lado;
        }
        public override short FormaTypeId { get; set; } = Constant.Circulo;
        //public override decimal Area { get => (decimal)Math.PI * (Lado / 2) * (Lado / 2);  }
        //public override decimal Perimetro { get =>  (decimal) Math.PI * Lado;  }
    }
}
