﻿using CodingChallenge.Common;

namespace CodingChallenge.Entity
{
    public class Cuadrado : FormaLado
    {
        public Cuadrado(decimal lado) {
            this.Lado = lado;
        }
        public override short FormaTypeId { get; set; } = Constant.Cuadrado;
        //public override decimal Area { get => Lado * Lado; }
        //public override decimal Perimetro { get => Lado * 4; }
    }
}
