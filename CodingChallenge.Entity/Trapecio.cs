﻿using CodingChallenge.Common;

namespace CodingChallenge.Entity
{
    public class Trapecio : FormaBase
    {
        public Trapecio(decimal ladoInferior, decimal ladoSuperior, decimal altura, decimal ladoIzquierdo, decimal ladoDerecho)
        {
            this.LadoInferior = ladoInferior;
            this.LadoSuperior = ladoSuperior;
            this.Altura = altura;
            this.LadoIzquierdo = ladoIzquierdo;
            this.LadoDerecho = ladoDerecho;
        }
        public override short FormaTypeId { get; set; } = Constant.Trapecio;
        public decimal LadoInferior { get; set; }
        public decimal LadoSuperior { get; set; }
        public decimal Altura { get; set; }
        public decimal LadoIzquierdo { get; set; }
        public decimal LadoDerecho { get; set; }

        //public override decimal Area { get => (LadoInferior + LadoSuperior / 2) * Altura; }
        //public override decimal Perimetro { get => LadoDerecho + LadoIzquierdo + LadoSuperior + LadoInferior; }
    }
}
