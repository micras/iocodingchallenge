﻿using CodingChallenge.Common;

namespace CodingChallenge.Entity
{
    public class TrianguloEquilatero : FormaLado
    {
        public TrianguloEquilatero(decimal lado)
        {
            this.Lado = lado;
        }
        public override short FormaTypeId { get; set; } = Constant.TrianguloEquilatero;
        //public override decimal Area { get => ((decimal)Math.Sqrt(3) / 4) * Lado * Lado; }
        //public override decimal Perimetro { get => Lado * 3; }
    }
}
