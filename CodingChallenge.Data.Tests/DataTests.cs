﻿using System;
using System.Collections.Generic;
using CodingChallenge.Common;
using CodingChallenge.Data.Classes;
using CodingChallenge.Entity;
using CodingChallenge.Logic;
using NUnit.Framework;

namespace CodingChallenge.Data.Tests
{
    [TestFixture]
    public class DataTests
    {
        [TestCase]
        public void TestResumenListaVaciaFrances()
        {
            Assert.AreEqual("<h1>Liste vide de formulaires!</h1>", PrintLogic.Imprimir(new List<FormaBase>(), Constant.Frances));
        }

        [TestCase]
        public void TestResumenListaVacia()
        {
            //Assert.AreEqual("<h1>Lista vacía de formas!</h1>", FormaGeometrica.Imprimir(new List<FormaGeometrica>(), 1));
            Assert.AreEqual("<h1>Lista vacía de formas!</h1>", PrintLogic.Imprimir(new List<FormaBase>(), Constant.Castellano));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnIngles()
        {
            //Assert.AreEqual("<h1>Empty list of shapes!</h1>", FormaGeometrica.Imprimir(new List<FormaGeometrica>(), 2));
            Assert.AreEqual("<h1>Empty list of shapes!</h1>", PrintLogic.Imprimir(new List<FormaBase>(), Constant.Ingles));
        }

        [TestCase]
        public void TestResumenListaConUnCuadrado()
        {
            //var cuadrados = new List<FormaGeometrica> {new FormaGeometrica(FormaGeometrica.Cuadrado, 5)};

            //var resumen = FormaGeometrica.Imprimir(cuadrados, FormaGeometrica.Castellano);
            var cuadrados = new List<FormaBase> {
                new Cuadrado(5),
            };
            var resumen = PrintLogic.Imprimir(cuadrados, Constant.Castellano);

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Cuadrado | Area 25 | Perímetro 20 <br/>TOTAL:<br/>1 formas Perímetro 20 Area 25", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasCuadrados()
        {
            //var cuadrados = new List<FormaGeometrica>
            //{
            //    new FormaGeometrica(FormaGeometrica.Cuadrado, 5),
            //    new FormaGeometrica(FormaGeometrica.Cuadrado, 1),
            //    new FormaGeometrica(FormaGeometrica.Cuadrado, 3)
            //};

            //var resumen = FormaGeometrica.Imprimir(cuadrados, FormaGeometrica.Ingles);
            var cuadrados = new List<FormaBase> {
                new Cuadrado(5),
                new Cuadrado(1),
                new Cuadrado(3)
            };
            var resumen = PrintLogic.Imprimir(cuadrados, Constant.Ingles);

            Assert.AreEqual("<h1>Shapes report</h1>3 Squares | Area 35 | Perimeter 36 <br/>TOTAL:<br/>3 shapes Perimeter 36 Area 35", resumen);
        }



        [TestCase]
        public void TestResumenListaConMasTipos()
        {
            //var formas = new List<FormaGeometrica>
            //{
            //    new FormaGeometrica(FormaGeometrica.Cuadrado, 5),
            //    new FormaGeometrica(FormaGeometrica.Circulo, 3),
            //    new FormaGeometrica(FormaGeometrica.TrianguloEquilatero, 4),
            //    new FormaGeometrica(FormaGeometrica.Cuadrado, 2),
            //    new FormaGeometrica(FormaGeometrica.TrianguloEquilatero, 9),
            //    new FormaGeometrica(FormaGeometrica.Circulo, 2.75m),
            //    new FormaGeometrica(FormaGeometrica.TrianguloEquilatero, 4.2m)
            //};

            //var resumen = FormaGeometrica.Imprimir(formas, FormaGeometrica.Ingles);

            //Nota: el separador en ingles es el .

            //Assert.AreEqual(
            //    "<h1>Shapes report</h1>2 Squares | Area 29 | Perimeter 28 <br/>2 Circles | Area 13,01 | Perimeter 18,06 <br/>3 Triangles | Area 49,64 | Perimeter 51,6 <br/>TOTAL:<br/>7 shapes Perimeter 97,66 Area 91,65",
            //    resumen);

            var formas = new List<FormaBase>
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Circulo(2.75M),
                new TrianguloEquilatero(4.2m),
            };

            var resumen = PrintLogic.Imprimir(formas, Constant.Ingles);
            Assert.AreEqual(
                "<h1>Shapes report</h1>2 Squares | Area 29 | Perimeter 28 <br/>2 Circles | Area 13.01 | Perimeter 18.06 <br/>3 Triangles | Area 49.64 | Perimeter 51.6 <br/>TOTAL:<br/>7 shapes Perimeter 97.66 Area 91.65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnCastellano()
        {
            //var formas = new List<FormaGeometrica>
            //{
            //    new FormaGeometrica(FormaGeometrica.Cuadrado, 5),
            //    new FormaGeometrica(FormaGeometrica.Circulo, 3),
            //    new FormaGeometrica(FormaGeometrica.TrianguloEquilatero, 4),
            //    new FormaGeometrica(FormaGeometrica.Cuadrado, 2),
            //    new FormaGeometrica(FormaGeometrica.TrianguloEquilatero, 9),
            //    new FormaGeometrica(FormaGeometrica.Circulo, 2.75m),
            //    new FormaGeometrica(FormaGeometrica.TrianguloEquilatero, 4.2m)
            //};

            //var resumen = FormaGeometrica.Imprimir(formas, FormaGeometrica.Castellano);

            var formas = new List<FormaBase>
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Circulo(2.75M),
                new TrianguloEquilatero(4.2m),
            };

            var resumen = PrintLogic.Imprimir(formas, Constant.Castellano);
            Assert.AreEqual(
                "<h1>Reporte de Formas</h1>2 Cuadrados | Area 29 | Perímetro 28 <br/>2 Círculos | Area 13,01 | Perímetro 18,06 <br/>3 Triángulos | Area 49,64 | Perímetro 51,6 <br/>TOTAL:<br/>7 formas Perímetro 97,66 Area 91,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConTiposAgregados()
        {
            var formas = new List<FormaBase>
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Trapecio(2,3,5,3,2),
                new Rectangulo(2,3),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Trapecio(1,2,3,2,1),
                new Rectangulo(2,4),
                new Circulo(2.75M),
                new TrianguloEquilatero(4.2m),
                new Trapecio(3,3,5,3,3),
                new Rectangulo(2,5),
            };

            var resumen = PrintLogic.Imprimir(formas, Constant.Ingles);
            Assert.AreEqual(
                "<h1>Shapes report</h1>2 Squares | Area 29 | Perimeter 28 <br/>2 Circles | Area 13.01 | Perimeter 18.06 <br/>3 Trapezoids | Area 46 | Perimeter 28 <br/>3 Triangles | Area 49.64 | Perimeter 51.6 <br/>3 Rectangles | Area 24 | Perimeter 36 <br/>TOTAL:<br/>13 shapes Perimeter 161.66 Area 161.65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConTiposAgregadosCastellano()
        {
            var formas = new List<FormaBase>
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Trapecio(2,3,5,3,2),
                new Rectangulo(2,3),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Trapecio(1,2,3,2,1),
                new Rectangulo(2,4),
                new Circulo(2.75M),
                new TrianguloEquilatero(4.2m),
                new Trapecio(3,3,5,3,3),
                new Rectangulo(2,5),
            };

            var resumen = PrintLogic.Imprimir(formas, Constant.Castellano);
            Assert.AreEqual(
                "<h1>Reporte de Formas</h1>2 Cuadrados | Area 29 | Perímetro 28 <br/>2 Círculos | Area 13,01 | Perímetro 18,06 <br/>3 Trapecios | Area 46 | Perímetro 28 <br/>3 Triángulos | Area 49,64 | Perímetro 51,6 <br/>3 Rectángulos | Area 24 | Perímetro 36 <br/>TOTAL:<br/>13 formas Perímetro 161,66 Area 161,65",
                resumen);
        }
    }
}
