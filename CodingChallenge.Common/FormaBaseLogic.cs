﻿namespace CodingChallenge.Common
{
    public class FormaBaseLogic<M> : IFormaBaseLogic<M> where M : class
    {
        public virtual decimal CalcularArea(M Forma)
        {
            throw new System.NotImplementedException();
        }

        public virtual decimal CalcularPerimetro(M Forma)
        {
            throw new System.NotImplementedException();
        }
    }
}
