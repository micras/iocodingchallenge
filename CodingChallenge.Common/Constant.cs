﻿namespace CodingChallenge.Common
{
    public class Constant
    {
        public const short Cuadrado = 1;
        public const short TrianguloEquilatero = 2;
        public const short Circulo = 3;
        public const short Trapecio = 4;
        public const short Rectangulo = 5;

        //Idiomas
        public const string Castellano = "es-AR";
        public const string Ingles = "en-US";
        public const string Frances = "fr-FR";
    }
}
