﻿namespace CodingChallenge.Common
{
    public interface IFormaBaseLogic<M> where M : class
    {
        decimal CalcularArea(M Forma);
        decimal CalcularPerimetro(M Forma);
    }
}
