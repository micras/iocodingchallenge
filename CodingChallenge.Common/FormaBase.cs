﻿namespace CodingChallenge.Common
{
    public class FormaBase
    {
        public virtual short FormaTypeId { get; set; } 
        public virtual decimal Area { get; set; }
        public virtual decimal Perimetro{ get; set; }

    }
}
