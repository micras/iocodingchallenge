﻿using Challenge.Language;
using CodingChallenge.Common;
using CodingChallenge.Entity;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace CodingChallenge.Logic
{
    public abstract class PrintLogic
    {
        public static string Imprimir(List<FormaBase> formas, string idioma)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo(idioma);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(idioma);

            var sb = new StringBuilder();
            if (!formas.Any())
            {
                sb.Append("<h1>" + StringResources.ListaVacia + "</h1>");
            }
            else
            {
                sb.Append("<h1>" + StringResources.ReporteFormas + "</h1>");

                CuadradoLogic cuadradoLogic = null;
                CirculoLogic circuloLogic = null;
                TrapecioLogic trapecioLogic = null;
                TrianguloEquilateroLogic trianguloEquilateroLogic = null;
                RectanguloLogic rectanguloLogic = null;

                var numeroCuadrados = 0;
                var numeroCirculos = 0;
                var numeroTriangulos = 0;
                var numeroTrapecios = 0;
                var numeroRectangulos = 0;

                var areaCuadrados = 0m;
                var areaCirculos = 0m;
                var areaTriangulos = 0m;
                var areaTrapecios = 0m;
                var areaRectangulos = 0m;

                var perimetroCuadrados = 0m;
                var perimetroCirculos = 0m;
                var perimetroTriangulos = 0m;
                var perimetroTrapecios = 0m;
                var perimetroRectangulos = 0m;


                foreach (var forma in formas)
                {
                    switch (forma.FormaTypeId)
                    {
                        case Constant.Cuadrado:
                            if (cuadradoLogic == null)
                                cuadradoLogic = new CuadradoLogic();
                            areaCuadrados += cuadradoLogic.CalcularArea(forma as Cuadrado);
                            perimetroCuadrados += cuadradoLogic.CalcularPerimetro(forma as Cuadrado);
                            numeroCuadrados++;
                            break;

                        case Constant.Circulo:
                            if (circuloLogic == null)
                                circuloLogic = new CirculoLogic();
                            areaCirculos += circuloLogic.CalcularArea(forma as Circulo);
                            perimetroCirculos += circuloLogic.CalcularPerimetro(forma as Circulo);
                            numeroCirculos++;
                            break;

                        case Constant.Trapecio:
                            if (trapecioLogic == null)
                                trapecioLogic = new TrapecioLogic();

                            areaTrapecios += trapecioLogic.CalcularArea(forma as Trapecio);
                            perimetroTrapecios += trapecioLogic.CalcularPerimetro(forma as Trapecio);
                            numeroTrapecios++;
                            break;

                        case Constant.TrianguloEquilatero:
                            if (trianguloEquilateroLogic == null)
                                trianguloEquilateroLogic = new TrianguloEquilateroLogic();

                            areaTriangulos += trianguloEquilateroLogic.CalcularArea(forma as TrianguloEquilatero);
                            perimetroTriangulos += trianguloEquilateroLogic.CalcularPerimetro(forma as TrianguloEquilatero);
                            numeroTriangulos++;
                            break;

                        case Constant.Rectangulo:
                            if (rectanguloLogic == null)
                                rectanguloLogic = new RectanguloLogic();

                            areaRectangulos += rectanguloLogic.CalcularArea(forma as Rectangulo);
                            perimetroRectangulos += rectanguloLogic.CalcularPerimetro(forma as Rectangulo);
                            numeroRectangulos++;
                            break;
                    }
                }

                if (numeroCuadrados > 0)
                    sb.Append(ObtenerLinea(numeroCuadrados, areaCuadrados, perimetroCuadrados, Constant.Cuadrado));
                if (numeroCirculos > 0)
                    sb.Append(ObtenerLinea(numeroCirculos, areaCirculos, perimetroCirculos, Constant.Circulo));
                if (numeroTrapecios > 0)
                    sb.Append(ObtenerLinea(numeroTrapecios, areaTrapecios, perimetroTrapecios, Constant.Trapecio));
                if (numeroTriangulos > 0)
                    sb.Append(ObtenerLinea(numeroTriangulos, areaTriangulos, perimetroTriangulos, Constant.TrianguloEquilatero));
                if (numeroRectangulos > 0)
                    sb.Append(ObtenerLinea(numeroRectangulos, areaRectangulos, perimetroRectangulos, Constant.Rectangulo));

                //FOOTER
                sb.Append("TOTAL:<br/>");
                sb.Append(numeroCuadrados + numeroCirculos + numeroTriangulos + numeroTrapecios + numeroRectangulos+ " " + StringResources.Formas + " ");
                sb.Append(StringResources.Perimetro + " " + (perimetroCuadrados + perimetroTriangulos + perimetroCirculos + perimetroTrapecios + perimetroRectangulos).ToString("##.##") + " ");
                sb.Append(StringResources.Area + " " + (areaCuadrados + areaCirculos + areaTriangulos + areaTrapecios + areaRectangulos).ToString("##.##"));
            }
            return sb.ToString();

        }

        private static string ObtenerLinea(int cantidad, decimal area, decimal perimetro, short tipo)
        {
            return $"{cantidad} {TraducirForma(tipo, cantidad)} | {StringResources.Area } { area:##.##} | {StringResources.Perimetro} { perimetro:##.##} <br/>";
        }

        private static string TraducirForma(short tipo, int cantidad)
        {
            switch (tipo)
            {
                case Constant.Cuadrado:
                    return cantidad == 1 ? StringResources.Cuadrado : StringResources.Cuadrados;
                case Constant.Circulo:
                    return cantidad == 1 ? StringResources.Circulo : StringResources.Circulos;
                case Constant.Trapecio:
                    return cantidad == 1 ? StringResources.Trapecio : StringResources.Trapecios;
                case Constant.TrianguloEquilatero:
                    return cantidad == 1 ? StringResources.Triangulo : StringResources.Triangulos;
                case Constant.Rectangulo:
                    return cantidad == 1 ? StringResources.Rectangulo : StringResources.Rectangulos;
            }
            return string.Empty;
        }
    }
}
