﻿using CodingChallenge.Common;
using CodingChallenge.Entity;
using System;

namespace CodingChallenge.Logic
{
    public class CirculoLogic : FormaBaseLogic<Circulo>
    {
        public override decimal CalcularArea(Circulo Forma)
        {
            return (decimal)Math.PI * (Forma.Lado / 2) * (Forma.Lado / 2);
        }

        public override decimal CalcularPerimetro(Circulo Forma)
        {
            return (decimal)Math.PI * Forma.Lado;
        }
    }
}
