﻿using CodingChallenge.Common;
using CodingChallenge.Entity;

namespace CodingChallenge.Logic
{
    public class RectanguloLogic : FormaBaseLogic<Rectangulo>
    {
        public override decimal CalcularArea(Rectangulo Forma)
        {

            return Forma.Base * Forma.Altura;
        }

        public override decimal CalcularPerimetro(Rectangulo Forma)
        {
            return (Forma.Base * 2) + (Forma.Altura * 2);
        }
    }
}
