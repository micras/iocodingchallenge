﻿using CodingChallenge.Common;
using CodingChallenge.Entity;

namespace CodingChallenge.Logic
{
    public  class CuadradoLogic : FormaBaseLogic<Cuadrado>
    {
        public override decimal CalcularArea(Cuadrado Forma)
        {
            
            return Forma.Lado * Forma.Lado;
        }

        public override decimal CalcularPerimetro(Cuadrado Forma)
        {
            return Forma.Lado * 4;
        }
    }
}
