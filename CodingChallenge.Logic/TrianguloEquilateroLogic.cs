﻿using CodingChallenge.Common;
using CodingChallenge.Entity;
using System;

namespace CodingChallenge.Logic
{
    public class TrianguloEquilateroLogic : FormaBaseLogic<TrianguloEquilatero>
    {
        public override decimal CalcularArea(TrianguloEquilatero Forma)
        {
            return ((decimal)Math.Sqrt(3) / 4) * Forma.Lado * Forma.Lado;
        }

        public override decimal CalcularPerimetro(TrianguloEquilatero Forma)
        {
            return Forma.Lado * 3;
        }
    }
}
