﻿using CodingChallenge.Common;
using CodingChallenge.Entity;
using System;

namespace CodingChallenge.Logic
{
    public class TrapecioLogic : FormaBaseLogic<Trapecio>
    {
        public override decimal CalcularArea(Trapecio Forma)
        {
            return (Forma.LadoInferior + Forma.LadoSuperior / 2) * Forma.Altura;
        }

        public override decimal CalcularPerimetro(Trapecio Forma)
        {
            return Forma.LadoDerecho + Forma.LadoIzquierdo + Forma.LadoInferior+ Forma.LadoSuperior;
        }
    }
}
